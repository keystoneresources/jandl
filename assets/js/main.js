$(document).ready(function() {

	$('footer').columnizeList({columnAmount:4});  

	
	if($('body.type-2').length){
	
		// Coordinate generator
 
		$('#nibs').click(function(e) {
	    var offset = $(this).offset();
	    var xcoord = Math.ceil(e.clientX - offset.left);
	    var ycoord = Math.ceil(e.clientY - offset.top); 
	    console.log('.nib{left:'+xcoord+'px;top:'+ycoord+'px;}');
	  });
	  
/*
	  $("header .learnmore").click(function () {
        this.toggle = !this.toggle;
        $("header .learnmore img").stop().fadeTo(400, this.toggle ? 0.1 : 1);
    });
	  
*/


	
	} 
	
	if ($('body.type-5').length){
    $('#contact_form').validate();
  }
  
  
  if ($('body.page-15').length || $('body.page-31').length || $('body.page-32').length){
  	//Add 6 products per page
  	
  	

  	var $children = $('#gallery .product:visible');
    for(var i = 0, l = $children.length; i < l; i += 8) {
      $children.slice(i, i+8).wrapAll('<div class="page"></div>');
    }
    //Create pagination   
    $('#gallery .page').each(function(index){
    	var pageId = index + 1; 
    	$(this).attr('data-id', pageId); 
	    $('.paging ul').append('<li data-id="'+pageId+'">'+pageId+'</li>');
    }); 
    $('#gallery .page:first').show();
    $('.paging').find('li:first').addClass('active');
    $('.paging ul li').click(function(){
    	var pageId = $(this).attr('data-id');
	    $('.paging ul li').removeClass('active');
	    $('.paging ul li[data-id="'+pageId+'"]').addClass('active');
	    $('#gallery .page[data-id="'+pageId+'"]').show();
	    $('#gallery .page:not([data-id="'+pageId+'"])').hide();
	    activateControls(); 
    });
    
    $('.paging').prepend("<div class='controls-left'><div class='first'>&laquo;</div><div class='prev'>&lsaquo;</div></div>");
  	$('.paging').append("<div class='controls-right'><div class='next'>&rsaquo;</div><div class='last'>&raquo;</div></div>");
  	

  	if($('.paging li:first').hasClass('active')) {
		   $('.paging .first').addClass('inactive');
		   $('.paging .prev').addClass('inactive');
		 }else{
		  $('.paging .first').removeClass('inactive');
		  $('.paging .prev').removeClass('inactive');
		 }
		 
		 if($('.paging li:last').hasClass('active')) {
		   $('.paging .next').addClass('inactive');
		   $('.paging .last').addClass('inactive');
		 }else{
		  $('.paging .next').removeClass('inactive');
		  $('.paging .last').removeClass('inactive');
		 }
  	
		//Use this function to update pagination 
		function updatePagination(){
			$('#gallery .page').contents().unwrap();
			var $children = $('#gallery .product:visible');
	    for(var i = 0, l = $children.length; i < l; i += 8) {
	      $children.slice(i, i+8).wrapAll('<div class="page"></div>');
	    }
	    var pageAmount = $('#gallery .page').length;
	    var activeId = parseInt($('.paging ul li.active').attr('data-id'));
	    $('#gallery .page').each(function(index){
	    	var pageId = index + 1; 
	    	$(this).attr('data-id', pageId); 
	    });
	    $('.paging ul li').each(function(){
	    	var pagingId = parseInt($(this).attr('data-id'));
		    if(pagingId > pageAmount){
			    $(this).hide();
		    }else{
			    $(this).show();
		    }
	    });
	    if(activeId > pageAmount){
		    $('#gallery .page:first').show();
				$('.paging ul').find('li:first').addClass('active');
	    }else{
	    	$('.paging ul li:not([data-id="'+activeId+'"])').removeClass('active');
		    $('#gallery .page[data-id="'+activeId+'"]').show();
	    }
		}
		
		function activateControls(){
			 if($('.paging li:first').hasClass('active')) {
		   $('.paging .first').addClass('inactive');
		   $('.paging .prev').addClass('inactive');
		 }else{
		  $('.paging .first').removeClass('inactive');
		  $('.paging .prev').removeClass('inactive');
		 }
		 
		 if($('.paging li:last').hasClass('active')) {
		   $('.paging .next').addClass('inactive');
		   $('.paging .last').addClass('inactive');
		 }else{
		  $('.paging .next').removeClass('inactive');
		  $('.paging .last').removeClass('inactive');
		 }
		}
		
		$('.paging .first').click(function(){
			$('.paging li').removeClass('active');
	  	$('.paging').each(function(){
		  	$(this).find('li:first').addClass('active');
	  	});
	  	updatePagination();
	  	activateControls();
  	});
  	
  	$('.paging .last').click(function(){
			$('.paging li').removeClass('active');
	  	$('.paging').each(function(){
		  	$(this).find('li:last').addClass('active');
	  	});
	  	updatePagination(); 
	  	activateControls();
  	});
		
		$('.paging .next').click(function(){
	  	var activePage = parseInt($('.paging li.active').attr("data-id"));
	  	if(!$('.paging li:last').hasClass('active')) {
			   var nextPage = activePage + 1;
		  	 $('.paging li.active[data-id="'+activePage+'"]').removeClass("active");
		  	 $('.paging li[data-id="'+nextPage+'"]').addClass("active"); 
		  	 updatePagination();
		  	 activateControls();
			 }
  	});
  	
  	$('.paging .prev').click(function(){
	  	var activePage = parseInt($('.paging li.active').attr("data-id"));
	  	if(activePage != 1){
		  	var nextPage = activePage - 1;
		  	console.log("The next page is "+nextPage);
		  	$('.paging li.active[data-id="'+activePage+'"]').removeClass("active");
		  	$('.paging li[data-id="'+nextPage+'"]').addClass("active"); 
		  	updatePagination();
		  	activateControls();
	  	}
  	});

	
		activeOptionArray = [];
		// If page has a hash from the homepage, show that stuff
    var currentHash = window.location.hash;
		if(currentHash != '' && currentHash != '#'){
			$('#options').removeClass('all');
			var option = currentHash.substr(1);
			window.location.hash = '';
			$('.icon[data-category="'+option+'"]').addClass('active');
			$('.product:not([data-category~="'+option+'"]').hide();
			updatePagination();
			activateControls();
			if(!$('#gallery .product:visible').length){
		    $('.paging').addClass('inactive');
	    }else{
			  $('.paging').removeClass('inactive');
		   }
		}//if hash

	
		$('.icon').each(function(){
	    $(this).click(function(){
				//update active icons
		    if($(this).hasClass('active')){
			    $(this).removeClass('active');
					$('.product').show();
		    }else{
			    $('.icon').removeClass('active');
			    $(this).addClass('active');
			    var clickedOption = $(this).attr('data-category');
			    $('.product:not([data-category~="'+clickedOption+'"])').hide();
			    $('.product[data-category~="'+clickedOption+'"]').show();
		    }//if
		    updatePagination();	
		    activateControls();
		    if(!$('#gallery .product:visible').length){
			    $('.paging').addClass('inactive');
		    }else{
			     $('.paging').removeClass('inactive');
		    }
	    });//click
    });//each
  }//if

  
  
  
  
  if ($('body.page-3').length){
  	//Add 6 products per page

  	var $children = $('#gallery .product:visible');
    for(var i = 0, l = $children.length; i < l; i += 8) {
      $children.slice(i, i+8).wrapAll('<div class="page"></div>');
    }
    //Create pagination   
    $('#gallery .page').each(function(index){
    	var pageId = index + 1; 
    	$(this).attr('data-id', pageId); 
	    $('.paging ul').append('<li data-id="'+pageId+'">'+pageId+'</li>');
    });
    $('#gallery .page:first').show();
    $('.paging').find('li:first').addClass('active');
    $('.paging ul li').click(function(){
    	var pageId = $(this).attr('data-id');
	    $('.paging ul li').removeClass('active');
	    $('.paging ul li[data-id="'+pageId+'"]').addClass('active');
	    $('#gallery .page[data-id="'+pageId+'"]').show();
	    $('#gallery .page:not([data-id="'+pageId+'"])').hide();
    });
 
		//Use this function to update pagination 
		function updatePagination(){
			$('#gallery .page').contents().unwrap();
			var $children = $('#gallery .product:visible');
	    for(var i = 0, l = $children.length; i < l; i += 8) {
	      $children.slice(i, i+8).wrapAll('<div class="page"></div>');
	    }
	    var pageAmount = $('#gallery .page').length;
	    var activeId = parseInt($('.paging ul li.active').attr('data-id'));
	    $('#gallery .page').each(function(index){
	    	var pageId = index + 1; 
	    	$(this).attr('data-id', pageId); 
	    });
	    $('.paging ul li').each(function(){
	    	var pagingId = parseInt($(this).attr('data-id'));
		    if(pagingId > pageAmount){
			    $(this).hide();
		    }else{
			    $(this).show();
		    }
	    });
	    if(activeId > pageAmount){
		    $('#gallery .page:first').show();
				$('.paging ul').find('li:first').addClass('active');
	    }else{
	    	$('.paging ul li:not([data-id="'+activeId+'"])').removeClass('active');
		    $('#gallery .page[data-id="'+activeId+'"]').show();
	    }
		}
		
		activeOptionArray = [];
		// If page has a hash from the homepage, show that stuff
    var currentHash = window.location.hash;
		if(currentHash != '' && currentHash != '#'){
			$('#options').removeClass('all');
			var options = currentHash.substr(1);
			window.location.hash = '';
			var activeOptionArray = options.split('-');
			var product = '.product';
			for(var i=0; i<activeOptionArray.length; ++i){
				product += ':not([data-category~="'+activeOptionArray[i]+'"])';
				$(product).hide();
				$('.icon[data-category="'+activeOptionArray[i]+'"]').addClass('active');
			}
			updatePagination();
		}//if hash
	
		$('.icon').each(function(){
	    $(this).click(function(){
				//update active icons
		    if($(this).hasClass('active')){
			    $(this).removeClass('active');
					var clickedOption = $(this).attr('data-category');
					activeOptionArray.splice($.inArray(clickedOption, activeOptionArray), 1);
					var product = '.product';
					for(var i=0; i<activeOptionArray.length; ++i)
					product += ':not([data-category~="'+activeOptionArray[i]+'"])';
					$(product+'[data-category~="'+clickedOption+'"]').hide();
		    }else{
			    $(this).addClass('active');
			    var clickedOption = $(this).attr('data-category');
			    activeOptionArray.push(clickedOption);
			    $('.product[data-category~="'+clickedOption+'"]').show();
			    if($('#options.all').length){
			    	$('#options').removeClass('all');
				    $('.product:not([data-category~="'+clickedOption+'"])').hide();
			    }
		    }//if
		    updatePagination();	
	    });//click
    });//each
  }//if
  
  if ($('body.type-9').length){
    $('#slideshow').cycle({
      speed: 1300,
      timeout: 5000,
      fx: 'fade',
      slides: '> div',
      width: '100%',
      fit: 1,
      pager: '.slideshow-pager'
    });
    $('#slideshow img').fadeIn();
  }
  
  
  
  
  
}); //End Document Ready

